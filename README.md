# BLFC_2020_CyberPunkBadge

This is the main repository for tracking the requirements, hardware, and software implementations for the Biggest Little Fur Con 2020 cyberpunk electronic attendee badge. 

The software is contained inside the `software-v1` git branch, while all the hardware is inside the `hardware-v1` git branch.